<?php
  $rss = simplexml_load_file('http://www.nasa.gov/rss/image_of_the_day.rss');
  $item = $rss->channel->item;
  $title = $rss->channel->item[0]->title;
  header("Content-type: image/png");
  $im = @imagecreate(150, 30) or die("Cannot Initialize new GD image stream");
  $background_color = imagecolorallocate($im, 255, 255, 255);
  $text_color = imagecolorallocate($im, 0, 0, 0);
  imagestring($im, 12, 5, 5,  $title, $text_color);
  imagepng($im);
  imagedestroy($im);
?>

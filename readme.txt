(Draft version)

The scenario:

After enabling path module and clean URL, when visiting a URL like http://example.com/path/to/directory, it is in fact visiting something like http://example.com/node/100. The path/to/index is just an alias to node/100. Hence, even if you set Directory Index correctly in Apache conf file, you still get a HTTP 404 error. It becomes a serious problem if you are migrating a website from static HTML extensive site to a Drupal powered site.

The module is designed to fix this issue. It will look up path/to/directory/index.html, path/to/directory/index.htm, etc (users can specify what files to search for) . If the system finds an alias like that, it will return that node.